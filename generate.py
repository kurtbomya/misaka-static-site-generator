import os
import misaka

BASE_DIR = os.getcwd()
CONTENT_DIR = os.path.join(BASE_DIR, 'content')
OUTPUT_DIR = os.path.join(BASE_DIR, 'output')


def get_all_posts():
    for item in os.listdir(CONTENT_DIR):
        if all([
            not item.startswith('.'),
            item.endswith('.md'),
            os.path.isfile(f'{CONTENT_DIR}/{item}'),
        ]):
            yield item


def generate_html(files):
    if not os.path.exists(OUTPUT_DIR):
        os.mkdir(OUTPUT_DIR)
    for file in files:
        with open(f'{CONTENT_DIR}/{file}') as f:
            content = misaka.html(f.read())
            new_filename = os.path.splitext(file)[0] + '.html'
            open(os.path.join(OUTPUT_DIR, new_filename), 'w').write(content)


def main():
    posts = get_all_posts()
    generate_html(posts)


if __name__ == '__main__':
    main()
